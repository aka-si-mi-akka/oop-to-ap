package com.wirecard;

import org.junit.Before;
import org.junit.Test;

import com.wirecard.domain.am.Dealer;
import com.wirecard.domain.am.Factory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;

public class ActorTest {
	private ActorSystem system;
	private TestKit factoryMock;

	@Before
	public void setup() {
		system = ActorSystem.create();
		factoryMock = new TestKit(system);
	}
	
	@Test
	public void testDealer() {
	  final ActorRef dealer = system.actorOf(Props.create(Dealer.class, () -> new Dealer(factoryMock.getRef())));
	  dealer.tell(Dealer.SellCar.INSTANCE, ActorRef.noSender());
	  factoryMock.expectMsgClass(Factory.DeliverCar.class);
	  factoryMock.reply(Factory.NEW_CAR);
	}
}
