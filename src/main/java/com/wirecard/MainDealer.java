package com.wirecard;

import com.typesafe.config.ConfigFactory;
import com.wirecard.domain.am.Dealer;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.FromConfig;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;

public class MainDealer {

  public static void main(String[] args) throws Exception {
    ActorSystem system = ActorSystem.create("my-system", ConfigFactory.parseResources("dealer.conf"));
    ActorRef factory = system.actorOf(FromConfig.getInstance().props(), "factory");
    ActorRef dealer = system.actorOf(Props.create(Dealer.class, () -> new Dealer(factory)), "dealer");
    System.out.println("Dealer is open");

    system.scheduler().schedule(
      java.time.Duration.ofSeconds(10),
      java.time.Duration.ofSeconds(1),
      dealer,
      Dealer.SellCar.INSTANCE,
      system.dispatcher(),
      ActorRef.noSender());

    Await.ready(system.whenTerminated(), Duration.Inf());
  }

}
