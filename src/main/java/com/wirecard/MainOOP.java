package com.wirecard;

import com.wirecard.domain.oop.Dealer;
import com.wirecard.domain.oop.Factory;

public class MainOOP {

  public static void main(String[] args) throws Exception {
    Factory f = new Factory();
    Dealer d = new Dealer(f);

    while(true) {
      f.makeCar();
      f.makeCar();
      d.sellCar();
      d.sellCar();
      d.sellCar();
      Thread.sleep(1000);
    }
  }
}
