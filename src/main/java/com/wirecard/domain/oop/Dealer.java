package com.wirecard.domain.oop;

public class Dealer {

  private Factory factory;

  public Dealer(Factory factory) {
    this.factory = factory;
  }

  public void sellCar() {
    String resp = factory.deliverCar();
    switch (resp) {
    case Factory.NEW_CAR:
      System.out.println("Dealer: enjoy your new car");
    case Factory.NO_CAR:
      System.out.println("Dealer: no cars available");
    }
  }
}
