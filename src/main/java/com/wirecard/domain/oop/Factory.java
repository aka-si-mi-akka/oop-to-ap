package com.wirecard.domain.oop;

public class Factory {

  public static final String NEW_CAR = "new car";
  public static final String NO_CAR = "no car";

  private int availableCars = 0;

  public void makeCar() {
    availableCars++;
    System.out.println("Factory: " + availableCars + " available cars");
  }

  public String deliverCar() {
    if (availableCars > 0) {
      availableCars--;
      return NEW_CAR;
    }
    return NO_CAR;
  }
}
