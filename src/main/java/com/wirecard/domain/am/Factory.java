package com.wirecard.domain.am;

import java.io.Serializable;

import akka.actor.AbstractActor;
import akka.japi.pf.ReceiveBuilder;

public class Factory extends AbstractActor {

  private int availableCars = 0;
  
  @Override
  public Receive createReceive() {
    return ReceiveBuilder.create()
        .match(MakeCar.class, s -> makeCar())
        .match(DeliverCar.class, s -> sender().tell(deliverCar(), self()))
        .build();
  }

  private void makeCar() {
    availableCars++;
    System.out.println("Factory: " + availableCars + " available cars");
  }

  private String deliverCar() {
    if (availableCars > 0) {
      availableCars--;
      return NEW_CAR;
    }
    return NO_CAR;
  }

  // protocol
  public static final class MakeCar implements Serializable {
    public static final MakeCar INSTANCE = new MakeCar();

    private MakeCar() {
    }
  }

  public static final class DeliverCar implements Serializable {
    public static final DeliverCar INSTANCE = new DeliverCar();

    private DeliverCar() {
    }
  }

  public static final String NEW_CAR = "new car";
  public static final String NO_CAR = "no car";
}
