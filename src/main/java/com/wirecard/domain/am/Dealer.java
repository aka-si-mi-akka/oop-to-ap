package com.wirecard.domain.am;

import java.io.Serializable;

import com.wirecard.domain.am.Factory.DeliverCar;
import com.wirecard.domain.oop.Factory;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.japi.pf.ReceiveBuilder;

public class Dealer extends AbstractActor {

  private ActorRef factory;

  public Dealer(ActorRef factory) {
    this.factory = factory;
  }

  private void sellCar() {
    factory.tell(DeliverCar.INSTANCE, self());
  }

  @Override
  public Receive createReceive() {
    return ReceiveBuilder.create()
      .match(SellCar.class, msg -> sellCar())
      .match(String.class, resp -> resp.equals(Factory.NEW_CAR), resp -> System.out.println("Dealer: enjoy your new car"))
      .match(String.class, resp -> resp.equals(Factory.NO_CAR), resp -> System.out.println("Dealer: no cars available"))
      .build();
  }

  // protocol
  public static final class SellCar implements Serializable {
    public static final SellCar INSTANCE = new SellCar();
    private SellCar() {
    }
  }
}
