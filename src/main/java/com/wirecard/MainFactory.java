package com.wirecard;

import com.typesafe.config.ConfigFactory;
import com.wirecard.domain.am.Factory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;

public class MainFactory {

  public static void main(String[] args) throws Exception {
    ActorSystem system = ActorSystem.create("my-system", ConfigFactory.parseResources("factory.conf"));
    ActorRef factory = system.actorOf(Props.create(Factory.class, Factory::new), "factory");
    System.out.println("Factory is open");
    
    system.scheduler().schedule(
      java.time.Duration.ofSeconds(10),
      java.time.Duration.ofSeconds(3),
      factory,
      Factory.MakeCar.INSTANCE,
      system.dispatcher(),
      ActorRef.noSender());

    Await.ready(system.whenTerminated(), Duration.Inf());

  }

}
