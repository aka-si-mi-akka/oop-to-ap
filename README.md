# oop-to-ap [![pipeline status](https://gitlab.com/aka-si-mi-akka/oop-to-ap/badges/master/pipeline.svg)](https://gitlab.com/aka-si-mi-akka/oop-to-ap/commits/master)



- run `MainFactory` with vm args `-Dakka.remote.netty.tcp.port=10000` and modify port for next runs
- run `MainDealer` with vm args `-Dakka.remote.netty.tcp.port=11000` and modify port for next runs